/*
 * Copyright (c) 2017, Logan Matthew Nichols
 * 
 * Permission to use, copy, modify, and/or distribute this software for any purpose with or without 
 * fee is hereby granted, provided that the above copyright notice and this permission notice  
 * appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS 
 * SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE 
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES 
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, 
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */
package patterncompress;

import compresstest.Binary;

/**
 *
 * @author sweylo
 */
public class PatternDatum {
    
    protected byte patternID;
    
    public static final byte ID_BIT_COUNT = 3;
    
    public static final byte ZERO_RUN_ID = 0;
    public static final byte ONE_RUN_ID = 1;
    public static final byte ZERO_RAW_ID = 2;
    public static final byte ONE_RAW_ID = 3;
    
    public PatternDatum() {}
    
    public PatternDatum(byte patternID) {
        this.patternID = patternID;
    }
    
    public Binary getBinaryID() {
        return Binary.decToBin(patternID, ID_BIT_COUNT, Binary.ZERO);
    }
    
    public Binary toBinary() {
        
        Binary bin = new Binary();
        String binString = this.toString().replaceAll(" ", "");
        
        for (int i = 0; i < binString.length(); i++) {
            bin.add(binString.charAt(i) == '1');
        }
        
        return bin;
        
    }
    
}
