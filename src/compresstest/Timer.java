/*
 * Copyright (c) 2017, Logan Matthew Nichols
 * 
 * Permission to use, copy, modify, and/or distribute this software for any purpose with or without 
 * fee is hereby granted, provided that the above copyright notice and this permission notice  
 * appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS 
 * SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE 
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES 
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, 
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */
package compresstest;

/**
 * to simulate a time/stopwatch
 * @author sweylo
 */
public class Timer {
	
	private long initTime;
	private long finalTime;
	
	public void start() {
		initTime = System.nanoTime();
	}
	
	public void stop() {
		finalTime = System.nanoTime();
	}
    
    public boolean isStarted() {
        return initTime != 0;
    }
    
    public boolean isStopped() {
        return finalTime != 0;
    }
	
	public double getElapsed(String unit) {
		
        // check to make sure the time has been started
		if (!isStarted() && !isStopped()) {
			System.err.println("Timer must be both started and stopped.");
			return -1.0;
		}
		
		int exp;
		
		switch (unit) {
			
			case "s":				// seconds
				exp = 9;
				break;
			
			case "ms":				// milliseconds
				exp = 6;
				break;
			
			case "us":				// microseconds
				exp = 3;
				break;
			
			case "ns": default:		// nanoseconds
				exp = 0;
			
		}
		
		return (finalTime - initTime) / Math.pow(10, exp);
		
	}
	
}
