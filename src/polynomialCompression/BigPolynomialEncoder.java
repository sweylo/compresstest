/*
 * Copyright (c) 2019, Logan Matthew Nichols
 * 
 * Permission to use, copy, modify, and/or distribute this software for any purpose with or without 
 * fee is hereby granted, provided that the above copyright notice and this permission notice  
 * appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS 
 * SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE 
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES 
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, 
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */
package polynomialCompression;

import compresstest.Binary;
import static compresstest.Out.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;

/**
 * Class to handle encoding of raw binary data using polynomial regression
 *
 * @author sweylo
 */
public class BigPolynomialEncoder {

	private static final boolean DEBUG_POLY_BLOCK_CREATION = true;
	
	/**
	 *
	 * @param binFile Binary object containing the data to be encoded
	 * @param bitBlockSize blocks of binary bits to separate the file's binary into
	 * @param polyBlockSize number of blocks to fit a single polynomial to
	 * @param maxDegreeRetry times to retry a polynomial degree if error is too high
	 * @param polyPrecision precision for BigDecimal division when fitting polynomials
	 * @return Binary object containing the encoded data
	 */
	public static Binary encode(Binary binFile, int bitBlockSize, int polyBlockSize,
		int maxDegreeRetry, int polyPrecision) {

		// instantiate an arraylist of polynomial fitters
		//ArrayList<BigPolynomial> polynomials = new ArrayList<>();

		// instantiate an arraylist for holding lists of bit-blocks polynomials will be fitted to
		ArrayList<ArrayList<BigInteger>> polyBlocks = new ArrayList<>();

		/**********************************************************************************************
			create poly-blocks from binary data
		***********************************************************************************************/
		
		// instantiate divisor to divide bit blocks by
		//BigInteger divisor = BigInteger.valueOf(2).pow(20);
		
		// define counter for number of poly-blocks, for debug output
		int polyBlockCount = 0;

		// define counter to loop through binary bits with
		int bitIndex = 0;

		// traverse through binary to create bit blocks
		while (bitIndex < binFile.size()) {

			// instantiate arraylist for holding the blocks of binary bits
			ArrayList<BigInteger> bitBlocks = new ArrayList<>();

			//if (DEBUG_POLY_BLOCK_CREATION)
				//stdOutHeading("polyBlock[" + polyBlockCount + "]");

			// loop for the number of bit-blocks per poly-block to create a poly-block
			for (int i = 0; i < polyBlockSize; i++) {

				// initalize the bit-block's value at 0
				BigInteger bitBlock = BigInteger.ZERO;

				//stdOut("this bitBlock binary: ", false);
				
				// loop for bits per bit-block to create a bit-block
				for (int j = 0; j < bitBlockSize; j++) {
					try {

						//stdOut(binFile.getInt(bitIndex + k) + "", false);
						
						// add the integer value of the bit (in place)
						bitBlock = bitBlock.add(binFile.getBigInt(bitIndex + j).multiply(BigInteger.valueOf(2)
							.pow(bitBlockSize - 1 - j)));

					} catch (java.lang.IndexOutOfBoundsException e) {
						// if at the end of the file break out of the loop, remainder of poly-block is zeros
						break;
					}
				}

				if (DEBUG_POLY_BLOCK_CREATION)
					stdOut(String.format("block(%3d) = %30d", polyBlockCount * polyBlockSize + i, bitBlock));

				// add this bitBlock to the list of bit-blocks
				bitBlocks.add(bitBlock);

				// increment bit index the number of bits traversed creating the bit-block
				bitIndex += bitBlockSize;

			}

			// add this list of bit-blocks (poly-bitBlock) to the list of poly-blocks
			polyBlocks.add(bitBlocks);
			
			// increment poly-block count
			polyBlockCount++;

		}

		/**********************************************************************************************
			shift bit blocks
		 **********************************************************************************************/
    
    newLine();
		
		BigInteger[][] shiftedBitBlocks = new BigInteger[polyBlockSize * polyBlocks.size()][2];
		//ArrayList<BigInteger> shifters = new ArrayList<>();
		
		int bitBlockIndex = 0;
		
		for (ArrayList<BigInteger> thisPolyBlock : polyBlocks) {
			for (BigInteger thisBitBlock : thisPolyBlock) {
				shiftedBitBlocks[bitBlockIndex][0] = thisBitBlock;
				bitBlockIndex++;
			}
		}
		
		int shiftCount = 0;
		int maxShiftCount = 20;
		//BigInteger minShiftValue = BigInteger.valueOf(128);
		//BigInteger avgBitBlockLength = BigInteger.ONE;
		//BigInteger expBase = BigInteger.valueOf(11);
		BigInteger divisor = BigInteger.valueOf(8);
		BigInteger expBaseNumer = BigInteger.valueOf(31);
		BigInteger expBaseDenom = BigInteger.valueOf(3);
		//BigDecimal expBase = new BigDecimal(expBaseNumer)
			//.divide(new BigDecimal(expBaseDenom), 2, BigDecimal.ROUND_HALF_EVEN);
		
		while (shiftCount < maxShiftCount/*&& !avgBitBlockLength.equals(BigInteger.ZERO)*/) {
    
			for (int i = 0; i < shiftedBitBlocks.length; i++) {

				BigInteger thisBitBlock = shiftedBitBlocks[i][0];
				BigInteger shiftedBitBlock;
				BigInteger shiftedBitBlockRem;
				//BigInteger revertedBitBlock;
				//int digitCount = log10(thisBitBlock);
				
				//stdOut(thisBitBlock.compareTo(BigInteger.ZERO) == 1 ? "true" : "false");

				/*shiftedBitBlock = thisBitBlock.compareTo(BigInteger.ZERO) == 1 
					? thisBitBlock.subtract(shiftValue)
					: thisBitBlock.add(shiftValue);*/
				/*shiftedBitBlock = thisBitBlock.compareTo(BigInteger.ZERO) == 1 
					? thisBitBlock.subtract(expBase.pow(digitCount).toBigInteger())
					: thisBitBlock.add(expBase.pow(digitCount).toBigInteger());*/
				shiftedBitBlock = thisBitBlock.divide(divisor);
				shiftedBitBlockRem = thisBitBlock.remainder(divisor);

				//shiftedBitBlocks.add(shiftedBitBlock);
				shiftedBitBlocks[i][0] = shiftedBitBlock;
				shiftedBitBlocks[i][1] = shiftedBitBlockRem;

				//revertedBitBlock = shiftedBitBlock.multiply(divisor);
				//revertedBitBlock = revertedBitBlock.add(minBitBlock.multiply(BigDecimal.valueOf(2)));

				//stdOut(String.format("shiftedBitBlock[%3d] = %24d | -%2d digits | %2d bits", 
					//i, shiftedBitBlock, digitCount, shiftedBitBlocks[i].bitLength()));
				stdOut(String.format("shiftedBitBlock[%3d] = %24d | -%2d remainder | %2d bits", 
					i, shiftedBitBlock, shiftedBitBlockRem, shiftedBitBlockRem.bitLength()));
				//stdOut(String.format("revertedBitBlock[%3d][%3d] = %40d", 
					//i, j, revertedBitBlock));
				//stdOut(String.format("originalBitBlock[%3d][%3d] = %40d", i, j, thisBitBlock));

			}
			
			shiftCount++;

			newLine();

		}
		
		newLine();
		
		// initialize minimum bit-bitBlock to the first bit-block
		int maxBitCount = shiftedBitBlocks[0][1].bitLength();

		// loop through every bit-block
		for (BigInteger[] thisShiftedBitBlock : shiftedBitBlocks) {
			if (thisShiftedBitBlock[1].bitLength() != 0 && thisShiftedBitBlock[1].bitLength() > maxBitCount)
				maxBitCount = thisShiftedBitBlock[1].bitLength();
		}
		
		stdOut("shiftedBitBlocks.length = " + shiftedBitBlocks.length);
		stdOut("maxShiftCount = " + maxShiftCount);
		stdOut("maxDigitCount = " + maxBitCount);
		
		stdOut("encode bits part 1 = " + shiftedBitBlocks.length * maxShiftCount * divisor.intValue());
		stdOut("encode bits part 2 = " + shiftedBitBlocks.length * maxBitCount	);
		stdOut("encode bits part 3 = " + BigInteger.valueOf(maxShiftCount).bitLength());
		stdOut("encode bits part 4 = " + expBaseNumer.bitLength());
		stdOut("encode bits part 5 = " + expBaseDenom.bitLength());
		
		stdOut("ideal encoded size = " + (
			shiftedBitBlocks.length * maxShiftCount * divisor.intValue()	// which shifts changed the digit count
			+ shiftedBitBlocks.length * maxBitCount// the final shifted blocks
			+ BigInteger.valueOf(maxShiftCount).bitLength()								// the number of shifts
			+ divisor.bitLength()																		// numerator of the exponent base
		));
		stdOut("original size = " + binFile.size());
		
		/*for (BigInteger shifter : shifters) {
			stdOut("shifter: " + shifter);
		}*/
		
		/*BigInteger remainderSum = BigInteger.ZERO;
		
		for (BigInteger remainder : remainders) {
			remainderSum = remainderSum.add(remainder);
		}
		
		BigDecimal remainderAvg = new BigDecimal(remainderSum).divide(
			BigDecimal.valueOf(remainders.size()), polyPrecision, BigDecimal.ROUND_HALF_EVEN);
		stdOut("remainder average = " + remainderAvg);
		
		for (BigInteger remainder : remainders) {
			stdOut("new remainder = " + new BigDecimal(remainder).subtract(remainderAvg));
		}*/

		/*// define boolean flag to determine if at the end of the file
		boolean isAboveErrorThreshold;
		
		// define boolean flag to determine if there have been too many degree retries
		boolean tooManyDegreeRetries = false;
		
		while (!tooManyDegreeRetries) {
			
			BigPolynomialFitter pf;
			BigPolynomial thisPolynomial;
			
			int thisPolynomialDegree = polyBlockSize - 1;
			int polynomialDegreeRetryCount = 0;
			
			// create polyblocks within the error threshold
			do {
				
				newLine();
				stdOutHeading("polyBlock[" + polyBlockCount + " (" + thisPolynomialDegree + ")]");
				newLine();
			
				// instantiate arraylist for holding the blocks of binary bits
				bitBlocks = new ArrayList<>();

				// instantiate polynomial fitter for this polyblock
				pf = new BigPolynomialFitter(thisPolynomialDegree);
				
				// reset flag
				isAboveErrorThreshold = false;

				// loop for the number of blocks in a polyblock
				for (int j = 0; j < polyBlockSize; j++) {
					
					// initalize the bitBlock's value at 0
					BigDecimal bitBlock = BigDecimal.ZERO;
					
					stdOut("this bitBlock binary: ", false);

					// loop through a specified number of bits to create a bitBlock
					for (int k = 0; k < bitBlockSize; k++) {
						try {
							// add the integer value of the bit (in place)
							stdOut(binFile.getInt(bitIndex + k) + "", false);
							bitBlock = bitBlock.add(binFile.getBigInt(bitIndex + k).multiply(BigDecimal.valueOf(2)
								.pow(bitBlockSize - 1 - k)));
						} catch (java.lang.IndexOutOfBoundsException e) {
							// need to make this bit cleaner
							break;
						}
					}
					
					newLine();
					stdOut("this bitBlock = " + bitBlock);
					newLine();

					//System.out.printf("real: %d, %d\n", j, bitBlock);

					// add this bitBlock to the list of bitblocks
					bitBlocks.add(bitBlock);
					
					// add this point to the polynomial fitter
					pf.addPoint(BigDecimal.valueOf(j), bitBlock);
					
					// increment iterrator for the number of bits traversed creating the bitblock
					bitIndex += bitBlockSize;

				}
				
				// get best fit for this polyBlock
				thisPolynomial = pf.getBestFit(polyPrecision);
								
				// check for error at each point
				for (int i = 0; i < polyBlockSize; i++) {
					
					System.out.printf("x = %d | y(real) = %42.0f | y(poly) = %60.16f | y(rounded) = %1.0f\n",
						i, bitBlocks.get(i), thisPolynomial.getY(i), roundToInt(thisPolynomial.getY(i)));
					
					boolean isError = ((BigDecimal) bitBlocks.get(i))
						.compareTo(roundToInt(thisPolynomial.getY(i))) != 0;
					
					// check if real value is equal to the (rounded) calculated value from the polynomial
					if (isError) {
						// if above error threshold
						
						// decrement iterrator by a polyblock worth of bits
						bitIndex -= polyBlockSize * bitBlockSize;
						
						// increment to the next polynomial degree for hopefully better accuracy
						thisPolynomialDegree++;
						
						// set error threshold flag to stay in the polyblock loop
						isAboveErrorThreshold = true;
						
						// increment retry counter
						polynomialDegreeRetryCount++;
						
						tooManyDegreeRetries = polynomialDegreeRetryCount > BigPolynomialEncoder;

						stdOut("above error threshold, retry # " + polynomialDegreeRetryCount
							+ " / " + BigPolynomialEncoder);
						
						// break out of the error check loop
						break;
						
					}
					
				}
			
			} while (isAboveErrorThreshold && !tooManyDegreeRetries);

			stdOut("\nthis polynomial = " + thisPolynomial + "\n");

			polyBlocks.add(bitBlocks);
			polynomials.add(thisPolynomial);
			polyBlockCount++;
			
		}
		
		if (tooManyDegreeRetries) {
			stdErr("\n\ntoo many degree retries\n");
			System.exit(2);
		}
		
		stdOut("polynomials: \n");

		for (BigPolynomial p : (ArrayList<BigPolynomial>) polynomials) {
			System.out.printf("y = %s\n", p);
		}
		
		newLine();
		
		//BigDecimal divisor = BigDecimal.valueOf(2).pow(62);
		//BigDecimal divisor = new BigDecimal("2305843009213693952");
		BigDecimal divisor = new BigDecimal("50000000");
		
		ArrayList<BigPolynomial> compressedPolynomials = new ArrayList<>();
		
		stdOut("compressed polynomials: \n");

		for (BigPolynomial p : (ArrayList<BigPolynomial>) polynomials) {
			BigPolynomial divP = p.divideCoefficients(divisor, 9);
			System.out.printf("y = %s\n", divP);
			compressedPolynomials.add(divP);
		}
		
		newLine();
		
		stdOut("decompressed polynomials: \n");

		int thisPolyBlockIndex = 0;
		
		for (BigPolynomial p : (ArrayList<BigPolynomial>) compressedPolynomials) {
			
			BigPolynomial multP = p.multiplyCoefficients(divisor);
			System.out.printf("f(x) = %s\n", multP);
			
			for (int i = 0; i < polyBlockSize; i++) {
				ArrayList<BigDecimal> thisPolyBlock = 
					(ArrayList<BigDecimal>) polyBlocks.get(thisPolyBlockIndex);
				stdOut("fReal(" + i + ") = " + thisPolyBlock.get(i), false);
				stdOut(" | fPoly(" + i + ") = " + roundToInt(multP.getY(i)));
				stdOut("fReal(" + i + ") = | fPoly(" + i + "): "
					+ (roundToInt(multP.getY(i)).compareTo(thisPolyBlock.get(i)) == 0 ? "true" : "false"));
			}
			
			newLine();
			
			thisPolyBlockIndex++;
			
		}
		
		newLine();*/
 /*BigDecimal minBitBlock = ((ArrayList<BigDecimal>) polyBlocks.get(0)).get(0);
		
		for (ArrayList<BigDecimal> thisPolyBlock : (ArrayList<ArrayList>) polyBlocks) {
			for (BigDecimal thisBitBlock : (ArrayList<BigDecimal>) thisPolyBlock) {
				stdOut("thisBitBlock = " + thisBitBlock);
				stdOut("thisBitBlockBin = " 
					+ Binary.bigIntToBin(thisBitBlock.toBigInteger(), bitBlockSize, false));
				if (thisBitBlock.compareTo(minBitBlock) == -1) {
					minBitBlock = thisBitBlock;
				}
			}
		}
		
		newLine();
		stdOut("minBitBlock = " + minBitBlock);
		newLine();
		
		for (ArrayList<BigDecimal> thisPolyBlock : (ArrayList<ArrayList>) polyBlocks) {
			for (BigDecimal thisBitBlock : (ArrayList<BigDecimal>) thisPolyBlock) {
				BigDecimal shiftedBitBlock = thisBitBlock.subtract(minBitBlock
					.multiply(BigDecimal.valueOf(1)));
				stdOut("thisBitBlock = " + shiftedBitBlock);
				stdOut("thisBitBlockBin = " 
					+ Binary.bigIntToBin(shiftedBitBlock.toBigInteger(), bitBlockSize, false));
			}
		}*/
 
		Binary encoded = new Binary();

		/*// string builder for constructing the encoded binary
		StringBuilder encoded = new StringBuilder();
		
		// add number of bits per bitBlock
		String bitBlockSizeBinStr = Long.toBinaryString(bitBlockSize);
		while (bitBlockSizeBinStr.length() < 16) {
			bitBlockSizeBinStr = "0" + bitBlockSizeBinStr;
		}
		encoded.append(bitBlockSizeBinStr);
		stdOut("bitBlockSize = " + bitBlockSizeBinStr);
		
		// add number of blocks per polynomial
		String polyBlockSizeBinStr = Long.toBinaryString(polyBlockSize);
		while (polyBlockSizeBinStr.length() < 4) {
			polyBlockSizeBinStr = "0" + polyBlockSizeBinStr;
		}
		encoded.append(polyBlockSizeBinStr);
		stdOut("polyBlockSize = " + polyBlockSizeBinStr);
		
		for (Polynomial p : (ArrayList<Polynomial>) polynomials) {
			encoded.append(Long.toBinaryString(p.size()));
			for (i = 0; i < p.size(); i++) {
				String binStr = Binary.doubleToBinStr(p.get(i));
				encoded.append(binStr);
			}
		}*/
    
		return encoded;

	}
	
	// from https://stackoverflow.com/questions/18828377/biginteger-count-the-number-of-decimal-digits-in-a-scalable-method
	private static int log10(BigInteger huge) {
    int digits = 0;
    int bits = huge.bitLength();
    // Serious reductions.
    while (bits > 4) {
      // 4 > log[2](10) so we should not reduce it too far.
      int reduce = bits / 4;
      // Divide by 10^reduce
      huge = huge.divide(BigInteger.TEN.pow(reduce));
      // Removed that many decimal digits.
      digits += reduce;
      // Recalculate bitLength
      bits = huge.bitLength();
    }
    // Now 4 bits or less - add 1 if necessary.
    if ( huge.intValue() > 9 ) {
      digits += 1;
    }
    return digits;
  }


	// modified from https://stackoverflow.com/questions/2808535/round-a-double-to-2-decimal-places
	private static BigDecimal roundToInt(BigDecimal value) {
		BigDecimal bd = value;
		bd = bd.setScale(0, RoundingMode.HALF_EVEN);
		return bd;
	}

}
