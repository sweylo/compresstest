/***************************************************************************
 *   Copyright (C) 2009 by Paul Lutus, Ian Clarke                          *
 *   lutusp@arachnoid.com, ian.clarke@gmail.com                            *
 *	                                                                       *
 *   Modified by Logan Nichols (sweylo)	in 2019                            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

package polynomialCompression;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

/**
 * A class to fit a polynomial to a (potentially very large) dataset.
 * 
 * @author Paul Lutus <lutusp@arachnoid.com>
 * @author Ian Clarke <ian.clarke@gmail.com>
 * @author Logan Nichols <sweylo@protonmail.com>
 * 
 */

/*
 * Changelog:
 * 20191004: Implement use of BigInteger and BigDecimal to increase precision and rename to match
 * 20191002: Move polynomial class to separate file, make code more readable by me (or due to my 
             OCD, however you want to look at it).
 * 20100130: Add note about relicensing
 * 20091114: Modify so that points can be added after the curve is
 *           created, also some other minor fixes 
 * 20091113: Extensively modified by Ian Clarke, main changes:
 *     - Should now be able to handle extremely large datasets
 *     - Use generic Java collections classes and interfaces 
 *       where possible
 *     - Data can be fed to the fitter as it is available, rather
 *       than all at once
 * 
 * The code that this is based on was obtained from: http://arachnoid.com/polysolve
 * 
 * Note: I (Ian Clarke) am happy to release this code under a more liberal 
 * license such as the LGPL, however Paul Lutus (the primary author) refuses
 * to do this on the grounds that the LGPL is not an open source license.  
 * If you want to try to explain to him that the LGPL is indeed an open 
 * source license, good luck - it's like talking to a brick wall.
 */
public class BigPolynomialFitter {

	private final int p, rs;
	private BigInteger n = BigInteger.ZERO;
	private final BigDecimal[][] m;
	private final BigDecimal[] mpc;
	
	/**
	 * @param degree
	 *            The degree of the polynomial to be fit to the data
	 */
	public BigPolynomialFitter(final int degree) {
		
		assert degree > 0;
		p = degree + 1;
		rs = 2 * p - 1;
		m = new BigDecimal[p][p + 1];
		mpc = new BigDecimal[rs];
		
		// initialize m array to all zeros
		for (int i = 0; i < m.length; i++) {
			for (int j = 0; j < m[i].length; j++) {
				m[i][j] = BigDecimal.ZERO;
			}
		}
		
		// initialize mps array to all zeros
		for (int i = 0; i < mpc.length; i++) {
			mpc[i] = BigDecimal.ZERO;
		}
		
	}

	/**
	 * Add a point to the set of points that the polynomial must be fit to
	 * 
	 * @param x The x coordinate of the point
	 * @param y The y coordinate of the point
	 */
	public void addPoint(final BigDecimal x, final BigDecimal y) {
		
		n = n.add(BigInteger.ONE);
		
		// process precalculation array
		for (int r = 1; r < rs; r++) {
			mpc[r] = mpc[r].add(x.pow(r));
		}
		
		// process RH column cells
		m[0][p] = m[0][p].add(y);
		for (int r = 1; r < p; r++) {
			m[r][p] = m[r][p].add(x.pow(r).multiply(y));
		}
		
	}

	/**
	 * Returns a polynomial that seeks to minimize the square of the total
	 * distance between the set of points and the polynomial.
	 * 
	 * @return A polynomial
	 */
	public BigPolynomial getBestFit(int precision) {
		
		//final BigDecimal[] mpcClone = mpc.clone();
		//final BigDecimal[][] mClone = new BigDecimal[m.length][];
		final BigDecimal[] mpcClone = mpc.clone();
		final BigDecimal[][] mClone = new BigDecimal[m.length][];
		
		for (int x = 0; x < mClone.length; x++) {
			mClone[x] = m[x].clone();
		}

		mpcClone[0] = mpcClone[0].add(new BigDecimal(n));
		
		// populate square matrix section
		for (int r = 0; r < p; r++) {
			for (int c = 0; c < p; c++) {
				mClone[r][c] = mpcClone[r + c];
			}
		}
		
		gj_echelonize(mClone, precision);
		final BigPolynomial result = new BigPolynomial(p);
		
		for (int j = 0; j < p; j++) {
			result.add(j, mClone[j][p]);
		}
		
		return result;
		
	}
	
	private BigDecimal fx(final BigDecimal x, final List<BigDecimal> terms) {
		
		BigDecimal a = BigDecimal.ZERO;
		int e = 0;
		
		for (final BigDecimal i : terms) {
			a = a.add(i.multiply(x.pow(e)));
			e++;
		}
		
		return a;
		
	}
	
	private void gj_divide(final BigDecimal[][] A, final int i, final int j, final int m, int precision) {
		
		for (int q = j + 1; q < m; q++) {
			A[i][q] = A[i][q].divide(A[i][j], precision, BigDecimal.ROUND_HALF_EVEN);
		}
		
		A[i][j] = BigDecimal.ONE;
		
	}

	private void gj_echelonize(final BigDecimal[][] A, int precision) {
		
		final int n = A.length;
		final int m = A[0].length;
		int i = 0;
		int j = 0;
		
		while (i < n && j < m) {
			
			int k = i;
			
			// look for a non-zero entry in col j at or below row i
			while (k < n && A[k][j].compareTo(BigDecimal.ZERO) == 0) {
				k++;
			}
			
			// if such an entry is found at row k
			if (k < n) {
				
				// if k is not i, then swap row i with row k
				if (k != i) {
					gj_swap(A, i, j);
				}
				
				// if A[i][j] is not 1, then divide row i by A[i][j]
				if (A[i][j].compareTo(BigDecimal.ONE) != 0) {
					gj_divide(A, i, j, m, precision);
				}
				
				// eliminate all other non-zero entries from col j by
				// subtracting from each
				// row (other than i) an appropriate multiple of row i
				gj_eliminate(A, i, j, n, m);
				
				i++;
				
			}
			
			j++;
			
		}
		
	}

	private void gj_eliminate(final BigDecimal[][] A, final int i, final int j, final int n, final int m) {
		for (int k = 0; k < n; k++) {
			if (k != i && A[k][j].compareTo(BigDecimal.ZERO) != 0) {
				
				for (int q = j + 1; q < m; q++) {
					A[k][q] = A[k][q].subtract(A[k][j].multiply(A[i][q]));
				}
				
				A[k][j] = BigDecimal.ZERO;
				
			}
		}
	}

	private void gj_swap(final BigDecimal[][] A, final int i, final int j) {
		BigDecimal temp[];
		temp = A[i];
		A[i] = A[j];
		A[j] = temp;
	}

}
