/*
 * Copyright (c) 2019, Logan Matthew Nichols
 * 
 * Permission to use, copy, modify, and/or distribute this software for any purpose with or without 
 * fee is hereby granted, provided that the above copyright notice and this permission notice  
 * appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS 
 * SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE 
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES 
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, 
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

package compresstest;

import static compresstest.Out.*;
import patterncompress.*;
import polynomialCompression.*;
//import org.mozilla.universalchardet.UniversalDetector;

/**
 * @author sweylo
 */
public class CompressTest {
    
	// test file constants
	public static final String TEST_INPUT_FILE_PATH = "assets/test_input/test.txt";
	public static final String TEST_INPUT_XSHORT_LIPSUM_PATH = "assets/test_input/lipsum_xshort.txt";
	public static final String TEST_INPUT_SHORT_LIPSUM_PATH = "assets/test_input/lipsum_short.txt";
	public static final String TEST_INPUT_LONG_LIPSUM_PATH = "assets/test_input/lipsum_long.txt";
	public static final String TEST_INPUT_PNG_IMAGE_PATH = "assets/test_input/moon.png";
	public static final String TEST_INPUT_RAW_IMAGE_PATH = "assets/test_input/IMG_2246.CR2";
	public static final String TEST_INPUT_JPG_IMAGE_PATH = "assets/test_input/small_image.jpg";
	public static final String TEST_OUTPUT_FILE_PATH = "assets/test_output/output.mithril";
	
	// testing parameter constants
	public static final int FILE_CHAR_ENCODING_LENGTH = 8;
	public static final boolean DEBUG_OUTPUT = true; // makes for substantial slowdown if true

	public static void main(String[] args) throws java.io.IOException {

		stdOutHeading("CompressTest.main()");
		
		// file encoding detection doesn't seem to work
		/*byte[] buf = new byte[4096];
		java.io.FileInputStream fis = new java.io.FileInputStream(TEST_INPUT_SHORT_LIPSUM_PATH);
		
		UniversalDetector detector = new UniversalDetector(null);
		
		int nread;
		while ((nread = fis.read(buf)) > 0 && !detector.isDone()) {
			detector.handleData(buf, 0, nread);
		}
		
		detector.dataEnd();
		
		String encoding = detector.getDetectedCharset();
		if (encoding != null) {
			System.out.println("Detected encoding = " + encoding);
		} else {
			System.out.println("No encoding detected.");
		}

		detector.reset();*/
		
		// read in the test file to Binary object
		Binary binFile = Binary.readFileToBinary(TEST_INPUT_SHORT_LIPSUM_PATH, 
			FILE_CHAR_ENCODING_LENGTH);

		newLine();
		stdOut("Original file binary:");
		
		//stdOut(binFile.toString());
		
		// instantiate and start a timer
		Timer timer = new Timer();
		timer.start();

		//patternCompressTest(binFile);
		polynomialCompressTest(binFile);
		
		// stop the timer and output elapsed time
		timer.stop();
		String timeUnit = "ms";
		System.out.printf("compute time: %6.3f %s\n", timer.getElapsed(timeUnit), timeUnit);

	}
		
	private static void polynomialCompressTest(Binary binFile) {
		
		stdOutHeading("CompressTest.polynomialCompressTest()");
		
		Binary encoded = BigPolynomialEncoder.encode(
			binFile, 
			63,				// bitBlockSize
			4,				// polyBlockSize
			6,				// maxDegreeRetry
			4					// polyPrecision
		);
		
		/*System.out.println("\n\nEncoded binary:\n");
		System.out.println(encoded);
		
		System.out.println(PolynomialDecoder.decode(encoded.toString()));
		
		System.out.println("encoded byte size = " + encoded.toString().length() / 8);*/
		
		System.out.println();
		
	}
	
	private static void patternCompressTest(Binary binFile) {

		DataList runRecord = binFile.getRuns();

		//System.out.println(runRecord);
		System.out.println("run avg: " + runRecord.getMean(6));
		System.out.println("run max: " + runRecord.getMax());

		PatternData pd = new PatternData(binFile, 4, 100, 12);
		Binary compressedData = pd.compress();

		//System.out.println("File contents: " + binFile);
		//System.out.printf("Original size: %d bits (%d bytes) \n", binFile.size(), binFile.size() / 8);

		System.out.printf("Compressed size: %d bits (%f bytes) \n", 
			compressedData.size(), (double) compressedData.size() / 8);
			//System.out.println(pd);
		System.out.println("Compressed contents: " + compressedData);

		// output compression ratio
		System.out.printf("Compression ratio: %6.3f:1 (%6.3f%%)\n", 
			(double) binFile.size() / compressedData.size(), 
			(double) compressedData.size() / binFile.size());
		
	}
    
}
