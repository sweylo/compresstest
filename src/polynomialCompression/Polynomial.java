/***************************************************************************
 *   Copyright (C) 2009 by Paul Lutus, Ian Clarke                          *
 *   lutusp@arachnoid.com, ian.clarke@gmail.com                            *
 *	                                                                         *
 *   Copyright (C) 2019 by Logan Nichols                                   *
 *   sweylo@protonmail.com                                                 *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

package polynomialCompression;

import java.util.ArrayList;

/**
 * A class to handle polynomials
 * 
 * @author Paul Lutus <lutusp@arachnoid.com>
 * @author Ian Clarke <ian.clarke@gmail.com>
 * @author Logan Nichols <sweylo@protonmail.com>
 * 
 */

public class Polynomial extends ArrayList<Double> {
	
	private static final long serialVersionUID = 1692843494322684190L;

	public Polynomial(final int p) {
		super(p);
	}

	public double getY(final double x) {
		
		double ret = 0;
		
		for (int p = 0; p < size(); p++) {
			ret += get(p) * (Math.pow(x, p));
		}
		
		return ret;
		
	}

	@Override
	public String toString() {
		
		final StringBuilder ret = new StringBuilder();
		
		for (int x = size() - 1; x > -1; x--) {
			ret.append(String.format("%40.20f", get(x)));
			ret.append(x > 0 ? String.format(" * x^%2d + ", x) : "");
		}
		
		return ret.toString();
		
	}
	
}
