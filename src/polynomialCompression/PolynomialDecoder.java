/*
 * Copyright (c) 2019, Logan Matthew Nichols
 * 
 * Permission to use, copy, modify, and/or distribute this software for any purpose with or without 
 * fee is hereby granted, provided that the above copyright notice and this permission notice  
 * appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS 
 * SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE 
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES 
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, 
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THIS SOFTWARE.
 */

package polynomialCompression;

import compresstest.Binary;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

/**
 * @author sweylo
 */
public class PolynomialDecoder {
	
	public static final byte DEGREE_VALUE_BITS = 2;
	public static final int POLYNOMIAL_COEFFICIENT_BITS = 64;
	
	public static String decode(String binStr) {
		
		ArrayList<Byte> encoded = stringToByteArrayList(binStr);
		StringBuilder decoded = new StringBuilder();
		
		int bitBlockSize = Binary.binStrToInt(binStr.substring(0, 16), 16);
		int polyBlockSize = Binary.binStrToInt(binStr.substring(15, 20), 4);
		
		int i = 20;
		
		while (i < encoded.size()) {
			
			// extract the current degree value to binary string
			String thisDegreeStr = "";
			for (byte j = 0; j < DEGREE_VALUE_BITS; j++) {
				thisDegreeStr += encoded.get(i + j);
			}
			
			i += DEGREE_VALUE_BITS;
			
			// convert the binary string degree to decimal byte
			byte thisDegree = Byte.parseByte(thisDegreeStr, 2);
			
			//System.out.println("thisDegree = " + thisDegree);
			
			// instantiate new polynomial with specified degree
			Polynomial thisPolynomial = new Polynomial(thisDegree - 1);
			
			// extract coefficients from the binary
			for (byte j = 0; j < thisDegree; j++) {
				
				byte[] thisPolynomialCoefficientArray = new byte[POLYNOMIAL_COEFFICIENT_BITS];
				for (byte k = 0; k < POLYNOMIAL_COEFFICIENT_BITS; k++) {
					thisPolynomialCoefficientArray[k] = encoded.get(i + k);
				}
				
				i += POLYNOMIAL_COEFFICIENT_BITS;
				
				double thisPolynomialCoefficient = Binary.binByteArrayToDouble(thisPolynomialCoefficientArray);
				//System.out.println("thisPolynomialCoefficient = " + thisPolynomialCoefficient);
				
				thisPolynomial.add(thisPolynomialCoefficient);
				
			}
			
			//System.out.println(thisPolynomial);
			
			// compute blocks
			for (byte j = 0; j < polyBlockSize; j++) {
				System.out.println(Binary.decToBin(round(thisPolynomial.getY(j), 0), bitBlockSize, false));
			}
			
		}
		
		return decoded.toString();
		
	}
	
	// from https://stackoverflow.com/questions/15331637/convert-string-to-arraylist-character-in-java
	private static ArrayList<Byte> stringToByteArrayList(String str) {
		
		ArrayList<Byte> bits = new ArrayList<>();
		
		for (char c : str.toCharArray()) {
			if (c == '1' || c == '0') {
				bits.add(Byte.parseByte(c + ""));
			} else {
				throw new IllegalArgumentException("String must only contain binary bit characters (1 or 0).");
			}
		}
		
		return bits;
		
	}
	
	// modified from https://stackoverflow.com/questions/2808535/round-a-double-to-2-decimal-places
	public static long round(double value, int places) {
		
		if (places < 0) throw new IllegalArgumentException();

		BigDecimal bd = BigDecimal.valueOf(value);
		bd = bd.setScale(places, RoundingMode.HALF_EVEN);
		return bd.longValue();
		
	}
	
}
